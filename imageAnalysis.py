
import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

#------ ------ ------ ------
# This code is intended for calculating glue coverage of flex+glass dummy assembly
# 2021 initial code - Silke Möbius (with input from Tobias Bisanz)
# 2022 polishing, added green screen function - Yusong Tian
# 2023 added README.md - Yusong Tian, Hua Ye
#------ ------ ------ ------

def binarize(image_to_transform, threshold, show, imgName, corrOpt):
	# now, lets convert that image to a single greyscale image using convert()
	output_image=image_to_transform.convert("L")
	# the threshold value is usually provided as a number between 0 and 255, which
	# is the number of bits in a byte.
	# the algorithm for the binarization is pretty simple, go through every pixel in the
	# image and, if it's greater than the threshold, turn it all the way up (255), and
	# if it's lower than the threshold, turn it all the way down (0).
	# so lets write this in code. First, we need to iterate over all of the pixels in the
	# image we want to work with
	# We also want to know the percentage that is black
	whitepixel = 0
	blackpixel = 0
	leftcut = 0.0#0.09 
	rightcut = 0.0#0.17 #0.16 for with vac picture
	uppercut = 0.0#0.12 #0.26 #0.17 values for larger area
	lowercut = 0.0#0.12#0.27 #0.25 for with vac picture, 0.19 values for larger area
	width, height=output_image.size
	#print "width, height: ",width, height
	num1=int(round(leftcut*width))
	num2=int(round((1-rightcut)*width))
	num3=int(round(uppercut*height))
	num4=int(round((1-lowercut)*height))
	for x in range(num1, num2):
		for y in range(num3, num4):
			# for the given pixel at w,h, lets check its value against the threshold
			if output_image.getpixel((x,y))< threshold: #note that the first parameter is actually a tuple object
				# lets set this to zero
				blackpixel += 1
				output_image.putpixel( (x,y), 0 )
			else:
				# otherwise lets set this to 255
				whitepixel += 1
				output_image.putpixel( (x,y), 255 )
	percentage_black = float(blackpixel)/float((blackpixel+whitepixel))*100
	pct_corrected=percentage_black
	if "corr" in corrOpt:
		pct_corrected=correctToPCBsize(percentage_black)
	print ("threshold "+str(threshold)+":",(percentage_black, blackpixel, whitepixel),"corrected:",pct_corrected)
	#output_image=output_image.convert("RGBA")
	#font = ImageFont.truetype("arial.ttf", 15, encoding="unic")
	#ImageDraw.Draw(output_image).text((100, 80),"Glue coverage = "+str(round(percentage_black))+'%',font=font,fill=(0,0,255,255))
	#now we just return the new image
	if 'save' in show:
		output_image.save(outputFolder+'/'+imgName+str(threshold)+'.jpg')
	if 'show' in show:
		output_image.show()


def analyseImage_greenScreen(image_to_transform, threshold, show, imgName, corrOpt):
	print (threshold)
	output_image=image_to_transform.convert("RGB")
	whitepixel = 0
	blackpixel = 0
	greenpixel = 0
	leftcut = 0.0#0.09 
	rightcut = 0.0#0.17 #0.16 for with vac picture
	uppercut = 0.0#0.12 #0.26 #0.17 values for larger area
	lowercut = 0.0#0.12#0.27 #0.25 for with vac picture, 0.19 values for larger area
	width, height=output_image.size
	#print "width, height: ",width, height
	num1=int(round(leftcut*width))
	num2=int(round((1-rightcut)*width))
	num3=int(round(uppercut*height))
	num4=int(round((1-lowercut)*height))
	#print "thePixel (5,5):",(output_image.getpixel((5,5)))#note that the first parameter is actually a tuple object

	for x in range(num1, num2):
		for y in range(num3, num4):
			r=(output_image.getpixel((x,y)))[0]
			g=(output_image.getpixel((x,y)))[1]
			b=(output_image.getpixel((x,y)))[2]
			if r<5 and g>=148 and g<=158 and b<5:
				greenpixel += 1
			else:
				gray=0.2989 * float(r) + 0.5870 * float(g) + 0.1140 * float(b) #convert to gray scale, function was found online
				#print "gray:",gray,"rgb:",r,g,b
				if gray<threshold:
					# set the rgb vlues to 0 (black)
					blackpixel += 1
					output_image.putpixel((x,y),(0,0,0))
				else:
					# otherwise set it to 255 (white)
					whitepixel += 1
					output_image.putpixel((x,y),(255,255,255))
	print ("threshold"+str(threshold)+", black, white, green:",blackpixel,whitepixel,greenpixel)
	if blackpixel+whitepixel!=0:
		percentage_black = float(blackpixel)/float((blackpixel+whitepixel))*100
		print ("black%, blackpixel, whitepixel:",percentage_black, blackpixel, whitepixel)
		pct_corrected=percentage_black
		if "corr" in corrOpt and "black" in corrOpt:
			pct_corrected=correctToPCBsize(percentage_black)
		print ("black% corrected:",pct_corrected)
	if blackpixel+whitepixel+greenpixel!=0:
		percentage_green = float(greenpixel)/float((blackpixel+whitepixel+greenpixel))*100
		pct_green_corrected=percentage_green
		if "corr" in corrOpt and "green" in corrOpt:
			pct_green_corrected=correctToPCBsize(percentage_green)
		print ("green%, green_corrected, greenpixel:",percentage_green,pct_green_corrected,greenpixel,"total pixel:",blackpixel+whitepixel+greenpixel)
	#now we just return the new image
	if 'save' in show:
		output_image.save(outputFolder+'/'+imgName+str(threshold)+'.jpg')
	if 'show' in show:
		output_image.show()


def analyseImage(imgName,threshold,inputFolder="", option="", corrOpt="correct"):
	img = Image.open(inputFolder+'/'+imgName) #, cv2.IMREAD_GRAYSCALE)
	print(imgName,"width, height:",img.size,"threshold:",threshold)
	#print(img.mode)
	#grayimg = img.convert('L')
	#grayimg.show()
	#print(grayimg.mode)
	#font = ImageFont.truetype(<font-file>, <font-size>)
	#font = ImageFont.truetype("sans-serif.ttf", 16)
	if "green" not in option:
		for i in range(threshold[0],threshold[1],threshold[2]):
			binarize(img, i, 'save',imgName,corrOpt)
			#binarize(img, threshold, 'saveshow') #130#140 for picture with vacuum
	else:
		for i in range(threshold[0],threshold[1],threshold[2]):
			analyseImage_greenScreen(img, i, 'save',imgName,corrOpt)


def correctToPCBsize(perc):
	newPerc=perc/(39.6*40.6)*(41*42)
	return newPerc


def calcCoverage(glueMass,thickness):
	glueThickness=sum(thickness)/len(thickness)
	coverage=glueMass/1.05/(glueThickness*0.001)/(39.6*40.6)
	print (glueMass,"glue thickness, coverage:",glueThickness, coverage)


#------ ------ main() ------ ------
inputFolder='/Users/ytian/hardware/2023-2_siteQualification/flex0306_glass7'
imgNames=[
"IMG_1122_cut_painted.jpg"
#"IMG_1122_cut_painted_2.jpg"
]
outputFolder='./'
#threshold=[110,160,5]
threshold=[120,127,2]
optGreen=""#"green"
optCorr="correct black"

for i in range(len(imgNames)):
	analyseImage(imgNames[i],threshold,inputFolder,optGreen,optCorr)

'''
glueMasses=[41.2,45.1,44.8,43.2]
thicknesses=[
[38.,38.,37.,39.],
[39.,38.,38.,38.],
[34.,32.,32.,33.],
[33.,30.,31.,33.]
]
for i in range(len(glueMasses)):
	calcCoverage(glueMasses[i],thicknesses[i])
'''



