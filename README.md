
# imageAnalysis.py
## intro
This code is used to get the coverage percentage from a clear photo of a flex glued with a glass dummy.

It will go through all pixels in the photo, if the color in greyscale is above a certain threshold specified by the user, the code makes the pixelr,g,b=(255,255,255). If the pixel is below threshold, it makes it (0,0,0). Then by counting the number of black pixels and divide by the total number of pixels, the code gives the coverage. It also gives a corrected coverage value, since the photo should have been cut according to the edge of the glass dummy, but the coverage should be calculated with the size of the centre square of the flex.

1. The input file should be a photo of your flex + glass dummy assembly, cropped to leave only the glass dummy, and the area with and without glue must be clearly separated. So, firstly, take a photo for the glued flex+glass from the side, with e.g. bright clear sky as background reflected in the glass. If the contrast is not good in the photo, the code will not give useful result
2. Using a picture editing software e.g. GIMP, cut out only the glass dummy part with the free selection tool
3. Make the picture rectangular: as the module shows like a trapezoid in the picture, use the Perspective tool in GIMP to reshape the  trapezoid to a proper rectangle
4. Specify these parameters in the code:
 - inputFolder
 - imgNames: this is a list, if there is only 1 photo to be analysed, one still need to specify it as an element in the list
 - outputFolder
 - threshold:  in the format [start, end, step]. End value is excluded. The code will produce binary photos with thresholds from start to end values, with an increment of the step value. The coverage, and the coverage that is corrected using the area of the flex size will be printed out in the terminal. TODO: save coverage in file.
 - optGreen: if you want to use the green screen functionality, include the word "green" in this option
 - optCorr: include the word "correct" in this option to correct the coverage according to the nominal size of the glass dummy and the flex body. If using green screen function, include both "correct" and "black" in this option
5. Run the code: ```python3 imageAnalysis.py```


### green screen
If you want to exclude any part of the image, similar to how the movie industry uses a green screen so that later they can get rid of the green part, mask the part you want to exclude in green with RGB value (0,153,0), and set optGreen="green"

### theoretical coverage
If you enter the glue thickness, the function calculates the theoretical coverage, assuming flat surfaces. So this is intended only for glass+glass, glass+steel samples.

## example
In the ```example``` folder there are a few pictures. Try to process ```IMG_1122.JPG```, and get ```IMG_1122_cut_painted.jpg124.jpg```.

After cutting and adjusting the perspective, you should get something similar to ```IMG_1122_cut.JPG```. You can then run the code on it.

If you're not satisfied by the resulting binary picture, you can also adjust the picture further using e.g. the "select by colour" tool and "Colours"->"Brightness-Contrast", and get something similar to ```IMG_1122_cut_painted.JPG```.

The resulting coverage the code gives would be around 68%.

The rest of the pictures are helpers if you want to try the green screen function.
